const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

let url = process.argv[2];
let stretch = process.argv.indexOf('stretch') > -1;
console.log(stretch);

function createWindow () {

    let displays = electron.screen.getAllDisplays();
    let width = stretch ? displays.map(display => display.workAreaSize.width).reduce((width1, width2) => width1 + width2) : displays[0].workAreaSize.width;
    let height = displays[0].workAreaSize.height;


  // Create the browser window.
  mainWindow = new BrowserWindow({enableLargerThanScreen: true, frame: false, alwaysOnTop: true});
  mainWindow.setBounds({
    x: 0,
    y: 0,
    width: width,
    height: height
  });
  // and load the index.html of the app.
  mainWindow.loadURL(url);


  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
